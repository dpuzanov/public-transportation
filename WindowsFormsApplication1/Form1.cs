﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tfl.Api.Presentation.Entities;
using TflWorker;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        private const string baseUrl = "https://maps.googleapis.com/maps/api/staticmap?";

        private const string initLocation = "51.5286416,-0.1015987";
        private const string initLatitude = "51.5286416";
        private const string initLongitude = "-0.1015987";

        private const int maxZoom = 16;
        private const int minZoom = 10;

        private const string size = "400x400";

        public Location Center { get; set; }

        //private List<Marker> markers;

        public List<Marker> Markers { get; set; }

        private int zoom;

        public int Zoom
        {
            get
            {
                return zoom;
            }

            set
            {
                if (value >= minZoom && value <= maxZoom)
                {
                    zoom = value;
                }
                else if (value < minZoom)
                {
                    zoom = minZoom;
                }
                else
                {
                    zoom = maxZoom;
                }
            }
        }

        public Form1()
        {
            InitializeComponent();
            this.Center = new Location(initLatitude, initLongitude);
            textBox_lat.Text = initLatitude;
            textBox_lon.Text = initLongitude;
            this.Zoom = minZoom;
            this.Markers = new List<Marker>();
            this.MapUpdate();
        }

        private void MapUpdate()
        {
            var fullUrl = new StringBuilder();
            fullUrl.Append(baseUrl);
            fullUrl.Append("center=" + this.Center.ToString());
            fullUrl.Append("&");
            fullUrl.Append("zoom=" + this.Zoom);
            fullUrl.Append("&");
            fullUrl.Append("size=" + size);
            lb_Url.Text = fullUrl.ToString();
            if (this.Markers.Count>0)
            {
                foreach (var marker in this.Markers)
                {
                    fullUrl.Append("&");
                    fullUrl.AppendFormat("markers=color:grey%7Clabel:{0}%7C{1}%7Csize:{2}", marker.Label, marker.Place.ToString(),marker.Size);
                }
            }
            webBrowser.Navigate(fullUrl.ToString());
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            var latitude = textBox_lat.Text;
            var longitude = textBox_lon.Text;
            this.Center.Latitude = latitude;
            this.Center.Longitude = longitude;
            MapUpdate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var worker = new TflWorkerClass();
            var list = worker.GetAllBikePlaces();
            foreach (var item in list)
            {
                var place =  new Location(item.Lat.ToString(),item.Lon.ToString());
                this.Markers.Add(new Marker(){Place = place, Label="B",Size="Mid"});
            }
            this.MapUpdate();
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.Zoom++;
            this.MapUpdate();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            this.Zoom--;
            this.MapUpdate();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            this.Center.MoveLeft(Zoom);
            this.MapUpdate();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            this.Center.MoveRight(Zoom);
            this.MapUpdate();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            this.Center.MoveUp(Zoom);
            this.MapUpdate();
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            this.Center.MoveDown(Zoom);
            this.MapUpdate();
        }
    }

    public class Location
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public Location(string latitude, string longtitude)
        {
            this.Latitude = latitude;
            this.Longitude = longtitude;
        }

        public override string ToString()
        {
            return this.Latitude + "," + this.Longitude;
        }

        public void MoveLeft(int zoomLevel)
        {
            double longtitude;
            if (Double.TryParse(this.Longitude, out longtitude))
            {
                longtitude = CalculateShift(zoomLevel, longtitude);
            }
            this.Longitude = longtitude.ToString();
        }

        public void MoveRight(int zoomLevel)
        {
            double longtitude;
            if (Double.TryParse(this.Longitude, out longtitude))
            {
                longtitude = CalculateShift(zoomLevel, longtitude, false);
            }
            this.Longitude = longtitude.ToString();
        }

        public void MoveDown(int zoomLevel)
        {
            double latitude;
            if (Double.TryParse(this.Latitude, out latitude))
            {
                latitude = CalculateShift(zoomLevel, latitude);
            }
            this.Latitude = latitude.ToString();
        }

        public void MoveUp(int zoomLevel)
        {
            double latitude;
            if (Double.TryParse(this.Latitude, out latitude))
            {
                latitude = CalculateShift(zoomLevel, latitude, false);
            }
            this.Latitude = latitude.ToString();
        }

        private static double CalculateShift(int zoomLevel, double param, bool decrease = true)
        {
            if (zoomLevel >= 10 && zoomLevel < 12)
            {
                param = decrease ? param -(double)(12 - zoomLevel) / 10 : param + (double)(12 - zoomLevel) / 10;
            }
            else if (zoomLevel >= 12 && zoomLevel < 15)
            {
                param = decrease ? param - 0.01 : param + 0.01;
            }
            else
            {
                param = decrease ? param - 0.001 : param + 0.001;
            }
            return param;
        }
    }

    public class Marker
    {
        public string Label { get; set; }
        public Location Place { get; set; }
        public string Size { get; set; }
    }
}
