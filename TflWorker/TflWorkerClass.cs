﻿using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Serializers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tfl.Api.Presentation.Entities;
using Newtonsoft.Json;

namespace TflWorker
{
    public class TflWorkerClass
    {
        private RestClient tflClient;

        private const string appId = "0056381f";
        private const string appKey = "6b5f3fdb73aa9af7b7f14ab8b1364b43";
        private const string uri = "https://api.tfl.gov.uk/";


        public TflWorkerClass()
        {
            this.tflClient = new RestClient(uri);
            //this.tflClient.AddDefaultHeader("Authorization", appKey);
            //this.tflClient.AddDefaultHeader("Application ID", appId);
            //this.tflClient.Authenticator = new HttpBasicAuthenticator(userName, password);
        }

        public IEnumerable<Place> GetAllBikePlaces()
        {
            var response = ExecuteGetRequest("BikePoint");
            var places = new List<Place>();
            places = JsonConvert.DeserializeObject<List<Place>>(response.Content);
            return places;
        }

        private IRestResponse ExecuteGetRequest(string resource, IDictionary<string, string> headers = null, IDictionary<string, string> properties = null)
        {
            var method = Method.GET;
            var requestFormat = DataFormat.Json;

            var request = new RestRequest { Method = method, Resource = resource, RequestFormat = requestFormat };

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.AddHeader(header.Key, header.Value);
                }
            }

            if (properties != null)
            {
                foreach (var property in properties)
                {
                    request.AddParameter(property.Key, property.Value);
                }
            }

            var response = this.tflClient.Execute(request);

            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception(String.Format("GET Request error: {0}", response.ErrorMessage));
            }

            return response;
        }

        private IRestResponse ExecutePutRequest(string resource, object body, IDictionary<string, string> headers = null, IDictionary<string, string> properties = null)
        {
            var method = Method.PUT;
            var requestFormat = DataFormat.Json;

            var request = new RestRequest { Method = method, Resource = resource, RequestFormat = requestFormat };

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.AddHeader(header.Key, header.Value);
                }
            }

            if (properties != null)
            {
                foreach (var property in properties)
                {
                    request.AddParameter(property.Key, property.Value);
                }
            }

            if (body != null)
            {
                request.AddBody(body);
            }

            var response = this.tflClient.Execute(request);

            if (response.ErrorMessage != null)
            {
                throw new Exception(String.Format("PUT Request error: {0}", response.ErrorMessage));
            }

            return response;
        }

        private IRestResponse ExecutePostRequest(string resource, object body, IDictionary<string, string> headers = null, IDictionary<string, string> properties = null)
        {
            var method = Method.POST;
            var requestFormat = DataFormat.Json;

            var request = new RestRequest { Method = method, Resource = resource, RequestFormat = requestFormat };

            if (headers != null)
            {
                foreach (var header in headers)
                {
                    request.AddHeader(header.Key, header.Value);
                }
            }

            if (properties != null)
            {
                foreach (var property in properties)
                {
                    request.AddParameter(property.Key, property.Value);
                }
            }

            if (body != null)
            {
                request.AddBody(body);
            }

            var response = this.tflClient.Execute(request);

            if (response.ErrorMessage != null)
            {
                throw new Exception(String.Format("POST Request error: {0}", response.ErrorMessage));
            }

            return response;
        }


    }
}
